package ru.javabit.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import ru.javabit.Application;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


//@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class CommentControllerIntegrationTest {
    private static final String ROOT = "http://localhost:8080/comment";
    private static final String ADD = "/add";
    private static final String GET_BY_NAME = "/getAllByName";
    private DFile dFile;
    private String dFileName = "C:\\test\\sdfsd";

    @Autowired
    private DFileDao dFileDao;

    org.slf4j.Logger logger = LoggerFactory.getLogger(CommentControllerIntegrationTest.class);

    private Comment prefillDfileWithComment(String commentText){
        Comment comment = new Comment();
        comment.setName(dFileName);
        comment.setCommentText(commentText);
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        dFile.setCommentList(commentList);
        return comment;
    }

    /**
     *
     * @param commentText
     * @return
     *POST
     * http://localhost:8080/comment/add?name=C%3A%5Ctest%5Csdfsd
     * curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:8080/comment/add?name=C%3A%5Ctest%5Csdfsd%5C' --data
     * '{
            "id":"",
            "name":"C:\\test\\sdfsd",
            "commentText":"some comment2eeeee",
            "lastEdited":[2019,9,21],
            "dFile":null
        }'
     */

    private Comment createComment(String commentText) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        Comment comment = prefillDfileWithComment(commentText);
        HttpEntity<Comment> entity = new HttpEntity<>(comment, headers);
        RestTemplate template = new RestTemplate();
        Comment createdComment = template.exchange(
                ROOT + ADD,
                HttpMethod.POST,
                entity,
                Comment.class
        ).getBody();

        assertNotNull(createdComment);
        assertEquals(commentText, createdComment.getCommentText());
        return createdComment;
    }

    /**
     *
     * @param
     * @return
     *
     * createComment()
     * GET
     * http://localhost:8080/comment/getAllByName?name=C%3A%5Ctest%5Csdfsd
     */
    @Test
    //@Transactional      //NOTE! transactional working with after and before but not with controllers
    public void checkAddComment() {


        Comment comment = createComment("some comment");
        Comment comment2 = createComment("some comment2");

        //не добавляется два комментария подряд

        String name = comment.getName();
        RestTemplate template = new RestTemplate();
        ResponseEntity<List<Comment>> responseEntity = template.exchange(
                ROOT + GET_BY_NAME + "?name={name}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Comment>>() {
                },
                name
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<Comment> commentList = responseEntity.getBody();
        logger.error("commentList.size()="+commentList.size());
        assertEquals("some comment", commentList.get(0).getCommentText());
        assertEquals("some comment2", commentList.get(1).getCommentText());
        //logger.error("commentList.get(0).getCommentText()="+commentList.get(0).getCommentText());
        //logger.error("commentList.get(1).getCommentText()="+commentList.get(1).getCommentText());
        assertNotNull(commentList);
    }

    @Before
    public void setUp() {
        dFile = new DFile(dFileName, "some comment", LocalDate.now(), true);
        dFileDao.add(dFile);
        DFile dFile2 = dFileDao.getByName(dFileName);
        logger.debug("setUp " + dFile2.getName() + dFile2.getLastUpdated());
    }

    @After
    public void tearDown() {
        dFileDao.deleteByName(dFileName);
    }
}