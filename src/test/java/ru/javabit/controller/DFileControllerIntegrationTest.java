package ru.javabit.controller;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.javabit.Application;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.DFile;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 *
 * NOTE! JS-encoded for URL get-path look like:
 * http://localhost:8080/dfile/all-child?name=C%3A%5Ctest%5Csdfsd%5C
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class DFileControllerIntegrationTest {

    private static final String ROOT = "http://localhost:8080/dfile";
    private static final String ADD = "/add";
    private static final String GET_BY_NAME = "/get";

    private static final String dFileName = "C:\\test\\sdfsd\\sdfsdfsdff";

    @Autowired
    private DFileDao dFileDao;

    private DFile prefillDfile(){
        DFile dFile = new DFile(dFileName, "some comment", LocalDate.now(), true);
        return dFile;
    }

    /**
     * start test only with app running
     *
     *
     * @param
     * @return
     *
     * createComment()
     * POST
     * http://localhost:8080/dfile/add?name=C%3A%5Ctest%5Csdfsd%5C
     * {
    "name":"C:\\test\\sdfsd\\fdsfsdfsdfg",
    "directory":true,
    "comment":"",
    "lastUpdated":null,
    "parentId":0,"commentList":[]
    }
     */
    private DFile createDFile() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        DFile dFile = prefillDfile();
        HttpEntity<DFile> entity = new HttpEntity<>(dFile, headers);
        RestTemplate template = new RestTemplate();
        DFile createdDFile = template.exchange(
                ROOT + ADD,
                HttpMethod.POST,
                entity,
                DFile.class
        ).getBody();
        assertNotNull(createdDFile);
        assertEquals("C:\\test\\sdfsd\\sdfsdfsdff", createdDFile.getName());
        return createdDFile;
    }

    /**
     * Get DFile entities from database
     */
    //TODO transactional annotation DO NOT 'work', must clear db after testing!
    @Test
    @Transactional
    public void checkAddDFile() {
        DFile dFile = createDFile();
        String name = dFile.getName();
        RestTemplate template = new RestTemplate();
        ResponseEntity<DFile> responseEntity = template.exchange(
                ROOT + GET_BY_NAME + "?name={name}",
                HttpMethod.GET,
                null,
                DFile.class,
                name
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        DFile dFile1 = responseEntity.getBody();
        assertEquals("C:\\test\\sdfsd\\sdfsdfsdff", dFile1.getName());
        assertNotNull(dFile1);
    }

    @After
    public void tearDown() {
        dFileDao.deleteByName(dFileName);
    }
}