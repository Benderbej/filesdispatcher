package ru.javabit.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.javabit.Application;
import ru.javabit.dao.CommentDao;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class DFileDaoIntegrationTest {

    org.slf4j.Logger logger = LoggerFactory.getLogger(CommentControllerIntegrationTest.class);

    private static final String dFileName = "C:\\test\\sdfsd\\sdfsdfsdff";
    private static DFile dFile;

    @Autowired
    private DFileDao dFileDao;
    @Autowired
    private CommentDao commentDao;

    @Test
    public void DfileCommentCascadeCreationDelitionTestDelete () {
        dFile = new DFile(dFileName, "some comment", LocalDate.now(), true);
        prefillComments();
        dFileDao.add(dFile);

        DFile resDFile = dFileDao.getByName(dFileName);
        assertNotNull(resDFile);
        assertEquals(2 , resDFile.getCommentList().size());
        assertEquals(2, commentDao.getDFileComments(dFileName).size());

        dFileDao.delete(dFile);
        assertNull(dFileDao.getByName(dFile.getName()));
        assertTrue(commentDao.getDFileComments(dFileName).size() == 0);
    }

    @Test
    public void DfileCommentCascadeCreationDelitionTestDeleteByName () {
        dFile = new DFile(dFileName, "some comment", LocalDate.now(), true);
        prefillComments();
        dFileDao.add(dFile);

        DFile resDFile = dFileDao.getByName(dFileName);
        assertNotNull(resDFile);
        assertEquals(2 , resDFile.getCommentList().size());
        assertEquals(2, commentDao.getDFileComments(dFileName).size());

        dFileDao.deleteByName(dFileName);
        assertNull(dFileDao.getByName(dFile.getName()));
        assertTrue(commentDao.getDFileComments(dFileName).size() == 0);
    }

    private void prefillComments() {
        Comment comment = new Comment();
        comment.setName(dFileName);
        comment.setCommentText("DFileDaoIntegrationTest comment");
        Comment comment2 = new Comment();
        comment2.setName(dFileName);
        comment2.setCommentText("DFileDaoIntegrationTest comment2");
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        commentList.add(comment2);
        dFile.setCommentList(commentList);
    }
}