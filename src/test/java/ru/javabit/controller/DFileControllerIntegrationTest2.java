package ru.javabit.controller;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.javabit.Application;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
public class DFileControllerIntegrationTest2 {

    private static final String ROOT = "http://localhost:8080/dfile";
    //private static final String GET_PARENT = "/get-parent";
    private static final String ADD = "/add";
    private static final String GET_BY_NAME = "/get";

    private String dFileName = "C:\\test\\sdfsd\\sdfsdfsdff";

    @Autowired
    private DFileDao dFileDao;

    private DFile prefillDfileWithComment(){
        DFile dFile = new DFile("C:\\test\\sdfsd\\sdfsdfsdff2", "some comment", LocalDate.now(), true);
        dFile.setCommentList(prefillCommentList("C:\\test\\sdfsd\\sdfsdfsdff2"));
        return dFile;
    }

    private List<Comment> prefillCommentList(String name){
        Comment comment = new Comment();
        comment.setName(name);
        comment.setCommentText("some comment");
        Comment comment2 = new Comment();
        comment.setName(name);
        comment.setCommentText("some comment2");
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        commentList.add(comment2);
        return commentList;
    }

    private DFile createDFileWithComments() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        DFile dFile = prefillDfileWithComment();
        HttpEntity<DFile> entity = new HttpEntity<>(dFile, headers);
        RestTemplate template = new RestTemplate();
        DFile createdDFile = template.exchange(
                ROOT + ADD,
                HttpMethod.POST,
                entity,
                DFile.class
        ).getBody();
        assertNotNull(createdDFile);
        //assertEquals("some comment", createdDFile.getCommentText());
        return createdDFile;
    }

    /**
     * Get DFile entities from database
     */
    //TODO transactional annotation DO NOT 'work', must clear db after testing!
//    @Test
//    @Transactional
    public void checkAddDFile() {
        //if(this.testEntityManager == null){logger.info("entityManager == null"); logger.error("entityManager == null");}
        //if(this.testEntityManager.getEntityManager() == null){logger.info("entityManager.getEntityManager() == null");}
        //this.testEntityManager.getEntityManager().createQuery("delete from comment");
        DFile dFile = createDFileWithComments();
        String name = dFile.getName();
        RestTemplate template = new RestTemplate();
        ResponseEntity<DFile> responseEntity = template.exchange(
                ROOT + GET_BY_NAME + "?name={name}",
                HttpMethod.GET,
                null,
                DFile.class,
                name
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        DFile dFile1 = responseEntity.getBody();
        assertEquals("C:\\test\\sdfsd\\sdfsdfsdff", dFile1.getName());
        assertNotNull(dFile1);
    }

//    @Test
//    @Transactional
    public void checkAddDFileWithComments() {
        DFile dFile = createDFileWithComments();
        String name = dFile.getName();
        RestTemplate template = new RestTemplate();
        ResponseEntity<List<Comment>> responseEntity = template.exchange(
                ROOT + GET_BY_NAME + "?name={name}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Comment>>() {
                },
                name
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<Comment> commentList = responseEntity.getBody();
        assertEquals("some comment", commentList.get(0).getCommentText());
        assertNotNull(commentList);
    }

//    @Test
//    private void getParent() {}
//
//    @Test
//    private void getAllChilds() {}

    @After
    public void tearDown() {
        dFileDao.deleteByName(dFileName);
    }
}