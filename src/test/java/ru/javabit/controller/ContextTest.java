package ru.javabit.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.javabit.config.AppConfig;
import ru.javabit.config.DataConfig;
import ru.javabit.config.jpa.HibernateConfig;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AppConfig.class, DataConfig.class, HibernateConfig.class})
//@DataJpaTest
//@AutoConfigureTestEntityManager
//@AutoConfigureTestDatabase
public class ContextTest {

    @Autowired
    private DFileDao dFileDao;

    org.slf4j.Logger logger = LoggerFactory.getLogger(CommentControllerIntegrationTest.class);

    //@Autowired
    //private TestEntityManager testEntityManager;

    //if(this.testEntityManager == null){logger.info("entityManager == null"); logger.error("entityManager == null");}
    //if(this.testEntityManager.getEntityManager() == null){logger.info("entityManager.getEntityManager() == null");}

    //this.testEntityManager.getEntityManager().createQuery("delete from comment");

    @Test
    public void testEx(){
        //assertNotNull(dFileDao.getByName("fffadd")); // TODO
        assertTrue(true);
    }




    private static final String ROOT = "http://localhost:8080/comment";
    private static final String ADD = "/add";
    private static final String GET_BY_NAME = "/getAllByName";

    private Comment prefillDfileWithComment(){
        DFile dFile = new DFile("C:\\test\\sdfsd", "some comment", LocalDate.now(), true);
        Comment comment = new Comment();
        comment.setName("C:\\test\\sdfsd");
        comment.setCommentText("some comment");
        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);
        dFile.setCommentList(commentList);
        return comment;
    }

    private Comment createComment() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        Comment comment = prefillDfileWithComment();
        HttpEntity<Comment> entity = new HttpEntity<>(comment, headers);
        RestTemplate template = new RestTemplate();
        Comment createdComment = template.exchange(
                ROOT + ADD,
                HttpMethod.POST,
                entity,
                Comment.class
        ).getBody();
        assertNotNull(createdComment);
        assertEquals("some comment", createdComment.getCommentText());
        return createdComment;
    }


    //TODO transactional anno DO NOT 'work', must clear db after testing!
    @Test
    @Transactional
    public void checkAddComment() {
        //if(this.testEntityManager == null){logger.info("entityManager == null"); logger.error("entityManager == null");}
        //if(this.testEntityManager.getEntityManager() == null){logger.info("entityManager.getEntityManager() == null");}
        //this.testEntityManager.getEntityManager().createQuery("delete from comment");
        Comment comment = createComment();
        String name = comment.getName();
        RestTemplate template = new RestTemplate();
        ResponseEntity<List<Comment>> responseEntity = template.exchange(
                ROOT + GET_BY_NAME + "?name={name}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Comment>>() {
                },
                name
        );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        List<Comment> commentList = responseEntity.getBody();
        assertEquals("some comment", commentList.get(0).getCommentText());
        assertNotNull(commentList);
    }





}


//// mockMvc
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = {AppConfig.class, DataConfig.class, HibernateConfig.class})
//public class ContextExampleTest {
//
//    @Autowired
//    private BarsukDao barsukDao;
//
//    @Test
//    public void testEx(){
//        assertNotNull(barsukDao.getByName("Jack"));
//    }
//}