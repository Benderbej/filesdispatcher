package ru.javabit.filesdispatch;

import java.io.File;
import java.util.Scanner;

public class DispatcherFilesTree {

    static int some = 5;

    public static void getFileTest(String url) {

        try {
            File dir = new File(url);
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (File tmpFile : files) {
                    //System.out.println("-" + tmpFile.getName() + "\n getAbsolutePath()=" + tmpFile.getAbsolutePath() + "\n tmpFile.getCanonicalPath()=" + tmpFile.getCanonicalPath() + " tmpFile.getPath()" + tmpFile.getPath());
                    System.out.println("-" + tmpFile.getName() + " tmpFile.getPath()" + tmpFile.getPath());
                    if (tmpFile.isDirectory()) {
                        File[] tmpFile1 = tmpFile.listFiles();
                        for (File file3 : tmpFile1) {
                            System.out.println("--" + file3.getName());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}