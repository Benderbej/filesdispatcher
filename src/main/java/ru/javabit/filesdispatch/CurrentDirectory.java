package ru.javabit.filesdispatch;

/**
 * Session scope bean
 * stores SQL table DFile id
 * and URL
 */


public class CurrentDirectory {

    private long currentDirId;

    private String currentDirUrl;

    public String getCurrentDirUrl() {
        return currentDirUrl;
    }

    public void setCurrentDirUrl(String currentDirUrl) {
        this.currentDirUrl = currentDirUrl;
    }

    public long getCurrentDirId() {
        return currentDirId;
    }

    public void setCurrentDirId(long currentDirId) {
        this.currentDirId = currentDirId;
    }
}