package ru.javabit.filesdispatch;

import ru.javabit.dto.TextEdition;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * textEditionMap - tempFileName, textEdition
 */

public class CurrentFilesOnEdit {

    private Map<String, AtomicReference<TextEdition>> textEditionMap;

    public CurrentFilesOnEdit() {
        this.textEditionMap = new HashMap<>();
    }

    public Map<String, AtomicReference<TextEdition>> getTextEditionMap() {
        return textEditionMap;
    }

    public void setTextEditionMap(Map<String, AtomicReference<TextEdition>> textEditionMap) {
        this.textEditionMap = textEditionMap;
    }
}