package ru.javabit.controller;

/**
 * first - check session, if have session var, use it
 * if no session set current directory as
 * @return INSERT SOME SMART JAVADOC TAG LINKING TO name's javadoc
 *
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.javabit.dto.TextEdition;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;
import ru.javabit.filesdispatch.DispatcherFilesTree;
import ru.javabit.service.DFileService;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
@PropertySource(value = "classpath:util.properties")
public class AppController {

    Logger logger = LoggerFactory.getLogger(AppController.class);

    public AppController(DFileService dFileService) {
        this.dFileService = dFileService;
    }

    private DFileService dFileService;
    protected EntityManager entityManager;
    private Environment environment;
    private String rootDirectory;//TODO relative paths
    List<DFile> dFileList;

    @Resource(name = "initCurrentDirectory")
    CurrentDirectory currentDirectory;

    @RequestMapping(method = RequestMethod.GET)
    public String hello(Model model) {
        init();
        dFileList = dFileService.getAllChildByName(currentDirectory.getCurrentDirUrl());
        model.addAttribute("currentUrl", currentDirectory.getCurrentDirUrl());
        setModelAttributes(model);
        return "index";
    }


    @GetMapping("/texteditor")
    public String initTextEditor(@RequestParam String name) {
        logger.error("initTextEditor()");
        logger.error("initTextEditor() fileName="+name);

        return "texteditor";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/texteditorr")
    @ResponseBody
    public String getTextEditor(@RequestBody(required = false) TextEdition textEdition) {
    logger.error("texteditorr");
        return "texteditorr";
    }

    private void init() {
        rootDirectory = getRootDirectory();
        if (!haveCurrentDirectory()) {
            if (!haveSQL()) {
                initSQL();
            }
            currentDirectory.setCurrentDirUrl(getRootDirectory() + "\\");
        }
        String path = currentDirectory.getCurrentDirUrl();
        currentDirectory.setCurrentDirUrl(path);
        setCurrentDirectory(currentDirectory);
    }

    private void initSQL() {
        Query q = entityManager.createQuery("from DFile");
        DFile dFile = new DFile(rootDirectory, "ROOT \\", null, true);
        dFileService.add(dFile);
    }

    private boolean haveSQL() {
        Query q = entityManager.createQuery("from DFile");
        System.err.println("location query" + q.toString());
        List<DFile> resultList = q.getResultList();
        return (!resultList.isEmpty()) ? true : false;
    }

    private boolean haveCurrentDirectory() {
        return (currentDirectory.getCurrentDirUrl() != null) ? true : false;
    }

    private void setModelAttributes(Model model) {
        model.addAttribute("rootDirectory",rootDirectory +"\\");
        model.addAttribute("currentDirectory",currentDirectory.getCurrentDirUrl());
        model.addAttribute("dFileList", dFileList);
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void setCurrentDirectory(CurrentDirectory currentDirectory) {
        this.currentDirectory = currentDirectory;
    }
}