package ru.javabit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;
import ru.javabit.service.DFileService;
import ru.javabit.service.FileService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Map;

@RestController
@RequestMapping("/file")
public class FilePatternController implements AbsToRelPaths {

    Logger logger = LoggerFactory.getLogger(FilePatternController.class);
    private FileService fileService;
    private Environment environment;
    private String rootDirectory;
    private int rootPathLength;

    @Resource(name = "initCurrentDirectory")
    CurrentDirectory currentDirectory;

    public FilePatternController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostConstruct
    public void initialize() {
        rootDirectory = getRootDirectory();
        rootPathLength = getRootDirectory().length();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DFile createFile(@RequestBody String data){
        logger.error("sreateFile");
        Map paramMap = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            paramMap = mapper.readValue(data, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("cant parse to create new file");
        }
        if (paramMap == null) {
            logger.error("not convenient params");
        }
        String fileName = (String) paramMap.get("fileName");
        String isD = (String) paramMap.get("isDir");
        boolean isDir = isD.equals("true")? true : false;
        String acc = (String) paramMap.get("access");
        BigInteger bi = new BigInteger(acc, 2);
        byte[] bytes = bi.toByteArray();
        byte access = bytes[0];
        return (DFile) filterRemoveRootPrefix(fileService.createFile(filterAddRootPrefix(fileName, currentDirectory.getCurrentDirUrl()), isDir, access), rootPathLength);
    }

    @PostMapping("/delete")
    @ResponseBody
    public boolean deleteFile(@RequestParam String name) {
        return fileService.deleteFileOrDir(filterAddRootPrefix(name, rootDirectory));
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public DFile submit(@RequestParam("file") MultipartFile file) {
        DFile dFile = fileService.uploadFile(file);
        return (DFile) filterRemoveRootPrefix(dFile, rootPathLength);
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }
}