package ru.javabit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.javabit.dto.EditWorkUnit;
import ru.javabit.dto.TextEdition;
import ru.javabit.entity.DFile;
import ru.javabit.service.DFileService;
import ru.javabit.service.TextEditorService;

@RestController
@RequestMapping("/textedit")
public class TextEditorController implements AbsToRelPaths{

    Logger logger = LoggerFactory.getLogger(TextEditorController.class);

    private TextEditorService textEditorService;

    public TextEditorController(TextEditorService textEditorService) {
        this.textEditorService = textEditorService;
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public TextEdition startTextEdition(@RequestBody TextEdition textEdition) {
        logger.debug("textEdition.getFileName()="+textEdition.getFileName() + textEdition.getText() + textEdition.getStatus() + textEdition.getEditorsNum() + textEdition.getCursorPos());
        return textEditorService.processTextEdition(textEdition);
    }

    @RequestMapping(value = "/modify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public TextEdition modifyTextEdition(@RequestBody TextEdition textEdition) {
        logger.debug("modifyTextEdition() textEdition.getFileName()="+textEdition.getFileName());
        return textEditorService.processTextEdition(textEdition);
    }

    @RequestMapping(value = "/end", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public TextEdition saveAndOutTextEdition(@RequestBody TextEdition textEdition) {
        logger.debug("saveAndOutTextEdition() textEdition.getFileName()="+textEdition.getFileName());
        return textEditorService.processTextEdition(textEdition);
    }
}