package ru.javabit.controller;

import ru.javabit.entity.HaveName;

import java.util.List;
import java.util.stream.Collectors;

public interface AbsToRelPaths<T extends HaveName> {

    default List<T> filterAddRootPrefix(List<T> tList, String prefix) {
        List<T> resList = tList.stream()
                .map(d -> {
                    T t = d;
                    t.setName(prefix.concat(d.getName()));
                    return t;
                }).collect(Collectors.toList());
        return  resList;
    }

    default List<T> filterRemoveRootPrefix(List<T> tList, int rootPathLength) {
        List<T> resList = tList.stream()
                .map(d -> {
                    T t = d;
                    String name = d.getName().substring(rootPathLength, d.getName().length());
                    t.setName(name);
                    return t;
                }).collect(Collectors.toList());
        return  resList;
    }

    default T filterAddRootPrefix(T t, String prefix){
        T res = t;
        res.setName(prefix.concat(t.getName()));
        return res;
    }

    default String filterAddRootPrefix(String name, String prefix){
        System.out.println(name + " " + prefix.concat(name));
        return prefix.concat(name);
    }

    default T filterRemoveRootPrefix(T t, int rootPathLength) {
        if (t != null && t.getName() != null) {
            String name = t.getName().substring(rootPathLength, t.getName().length());
            t.setName(name);
            return t;
        }
        return null;
    }
}