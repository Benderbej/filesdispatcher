package ru.javabit.controller;

/**
 *  Shows files and directories at the Current node
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;
import ru.javabit.service.DFileService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.sql.SQLTransactionRollbackException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dfile")
public class DFileController implements AbsToRelPaths {

    Logger logger = LoggerFactory.getLogger(DFileController.class);

    private DFileService dFileService;
    private Environment environment;
    private String rootDirectory;
    private int rootPathLength;

    @Resource(name = "initCurrentDirectory")
    CurrentDirectory currentDirectory;

    @PostConstruct
    public void initialize() {
        rootDirectory = getRootDirectory();
        rootPathLength = getRootDirectory().length();
    }

    public DFileController(DFileService dFileService) {
        this.dFileService = dFileService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DFile addDFile(@RequestBody DFile dFile) {
        logger.debug("/dfile/add dFile.getName()="+dFile.getName());
        return  (DFile) filterRemoveRootPrefix(dFileService.add((DFile) filterAddRootPrefix(dFile, rootDirectory)), rootPathLength);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DFile updateDFile(@RequestBody DFile dFile) {
        return  (DFile) filterRemoveRootPrefix(dFileService.update((DFile) filterAddRootPrefix(dFile, rootDirectory)), rootPathLength);
    }

    @GetMapping("/delete")
    @ResponseBody
    public DFile deleteDFile(@RequestParam String name) {
        return (DFile) filterRemoveRootPrefix(dFileService.delete(filterAddRootPrefix(name, rootDirectory)), rootPathLength);
    }

    @GetMapping("/get")
    @ResponseBody
    public DFile getDFileByName(@RequestParam String name) {
        return (DFile) filterRemoveRootPrefix(dFileService.getByName(filterAddRootPrefix(name, rootDirectory)), rootPathLength);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<DFile> getAllDFiles() {
        return filterRemoveRootPrefix(dFileService.getAllDFiles(), rootPathLength);
    }

    /**
     * get all node's childs - main method to get list of files at frontend
     * @param name - url-path of node
     * @return
     */
    @GetMapping("/all-child")
    @ResponseBody
    public List<DFile> getAllChildByName(@RequestParam String name) {
        if(!name.equals(rootDirectory.concat(File.separator))){
            //logger.info("rootDirectory="+rootDirectory+" ");
            name = filterAddRootPrefix(name, rootDirectory);
        }
        currentDirectory.setCurrentDirUrl(name);
        List<DFile> dFileList = dFileService.getAllChildByName(name);
        List<DFile> dFiles = insertIconsToDFiles(dFileList);
        return filterRemoveRootPrefix(dFiles, rootPathLength);
    }

    private List<DFile>  insertIconsToDFiles(List<DFile> dFileList) {
        List<DFile> resDFileList = new ArrayList<>();
        for (DFile dFile: dFileList) {
            String extensionIconClass = "icon-ext-non";
            String editableClass = "";
            if(dFile.getDirectory() == true){
                dFile.setExtensionClassName("fdir");
                dFile.setEditableClass("");
            } else {
                String fileArray[]=dFile.getName().split("\\.");
                String extension = fileArray[fileArray.length-1]; //Will print the file extension
                //logger.info("extension="+extension);
                switch (extension){
                    case "exe":
                        extensionIconClass = "fexe";
                        editableClass = "";
                    break;
                    case "txt":
                        extensionIconClass = "ftxt";
                        editableClass = "editable";
                    break;
                    case "doc":
                        extensionIconClass = "fdoc";
                        editableClass = "editable";
                    break;
                    default:
                        extensionIconClass = "fnon";
                        editableClass = "";
                }
            }
            dFile.setExtensionClassName(extensionIconClass);
            dFile.setEditableClass(editableClass);
            resDFileList.add(dFile);
        }
        return resDFileList;//TODO abstorel
        //return filterRemoveRootPrefix(resDFileList, rootPathLength);
    }

    @GetMapping("/get-parent")
    @ResponseBody
    public DFile getParentName(@RequestParam String name) {
        logger.debug("/get-parent "+name);
        String s = filterAddRootPrefix(name, rootDirectory);
        DFile d = dFileService.getParentName(s);
        DFile d2 = (DFile) filterRemoveRootPrefix(d, rootPathLength);
        return d2;
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }
}