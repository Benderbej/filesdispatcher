package ru.javabit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.javabit.entity.Comment;
import ru.javabit.service.CommentService;
import ru.javabit.service.DFileService;
import javax.annotation.PostConstruct;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController implements AbsToRelPaths {

    Logger logger = LoggerFactory.getLogger(CommentController.class);

    private CommentService commentService;
    private DFileService dFileService;
    private Environment environment;
    private String rootDirectory;
    private int rootPathLength;

    @PostConstruct
    public void initialize() {
        rootDirectory = getRootDirectory();
        rootPathLength = getRootDirectory().length();
    }

    public CommentController(CommentService commentService, DFileService dFileService) {
        this.commentService = commentService;
        this.dFileService = dFileService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Comment addComment(@RequestBody Comment comment) {
        return (Comment) filterRemoveRootPrefix((Comment) filterAddRootPrefix(comment, rootDirectory), rootPathLength);
    }

    @RequestMapping(value = "/addandgetall", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<Comment> addCommentAndGetAll(@RequestBody Comment comment) {
        comment = (Comment) filterAddRootPrefix(comment, rootDirectory);
        return filterRemoveRootPrefix(dFileService.addCommentAndGetAll(comment), rootPathLength);
    }

    @RequestMapping(value = "/getAllByName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<Comment> getAllByDFileName(@RequestParam String name) {
        logger.info("name="+name);
        return commentService.getAllCommentsByName(name);//TODO abstorel
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }
}