package ru.javabit.dao;

import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.util.List;
import java.util.Map;

public interface DFileDao extends BasicDao<DFile> {
    DFile getByName(String name);

    List<Object[]> findChildDFiles(String name);

    void insertDFiles(Map<String, DFile> fileMap);

    void deleteDFiles(List<String> dFileListToDel);

    List<DFile> getAll();

    List<Comment> addCommentAndGetAll(String name, String commentText);
}