package ru.javabit.dao;

import org.springframework.web.multipart.MultipartFile;
import ru.javabit.dto.TextEdition;
import ru.javabit.entity.DFile;
import ru.javabit.entity.FilePattern;

import java.io.File;

public interface FilePatternDao<T>{

    DFile uploadFile(MultipartFile file);

    DFile createFile(String filename, Boolean isDir, byte access);

    boolean deleteFileOrDir(String filename);

    TextEdition createTempFile(TextEdition textEdition);

    TextEdition updateTempFile(TextEdition textEdition, TextEdition curTextEdition);

    TextEdition replaceFileByTempFile(TextEdition textEdition);
}
