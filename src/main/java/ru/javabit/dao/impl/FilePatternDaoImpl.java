package ru.javabit.dao.impl;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.apache.commons.io.FileUtils;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;
import ru.javabit.controller.AppController;
import ru.javabit.dao.FilePatternDao;
import ru.javabit.dto.EditWorkUnit;
import ru.javabit.dto.TextEdition;
import ru.javabit.entity.DFile;
import ru.javabit.entity.FilePattern;
import ru.javabit.filesdispatch.CurrentDirectory;

import javax.annotation.Resource;
import java.io.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermissions;
import java.time.LocalDate;


public class FilePatternDaoImpl implements FilePatternDao<FilePattern>{

    private Environment environment;
    private final Class<FilePattern> entityClass;
    protected SessionFactory sessionFactory;
    private String rootDirectory;

    @Resource(name = "initCurrentDirectory")
    CurrentDirectory currentDirectory;

    public FilePatternDaoImpl(Class<FilePattern> entityClass) {
        this.entityClass = entityClass;
    }

    private static Logger logger = LoggerFactory.getLogger(FilePatternDaoImpl.class);

    public DFile uploadFile(MultipartFile file) {
        logger.error("uploading... "+file.getName());
        String path = currentDirectory.getCurrentDirUrl();
        String name = path + file.getOriginalFilename();
        Boolean isDir = false;
        LocalDate date = LocalDate.now();
        logger.error("path + name="+path + file.getOriginalFilename());
        try {
            file.transferTo(new File(path + file.getOriginalFilename()));
            return new DFile(name, "", date, isDir);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("cant transfer uploaded file");
        }
        return null;
    }


    /**
     * @param isDir
     * @param filename
     */
    public DFile createFile(String filename, Boolean isDir, byte access) {

//        logger.info("fileName="+filename);
//        logger.info("isDir="+isDir);
//        logger.info("access="+access);

        DFile dFile = null;
        Path dir = null;
        Path newFilePath = null;
        try {
            logger.debug("filename="+filename);
            dir = Paths.get(getCurrentDirectory().getCurrentDirUrl());
            Path fileToCreatePath = dir.resolve(filename);
            logger.info("File to create path: " + fileToCreatePath);
            if(!isDir){
                newFilePath = Files.createFile(fileToCreatePath);//attributes read write execute
                logger.info("New file created: " + newFilePath);
                dFile = new DFile(newFilePath.toString(), "comment", LocalDate.now(), isDir);
            } else {
                newFilePath = Files.createDirectory(fileToCreatePath);
                logger.info("New dir created: " + newFilePath);
                dFile = new DFile(newFilePath.toString(), "comment", LocalDate.now(), isDir);
            }
            logger.info("New File exits: " + Files.exists(newFilePath));

        } catch (IOException e) {
            logger.error("cant create new file");
            e.printStackTrace();
        }
        if(newFilePath != null) {
            setPermissions(newFilePath, access);
        } else {
            logger.error("newFilePath is null");
        }
        return dFile;
    }

    public boolean deleteFileOrDir(String filename) {
        logger.info(filename);
        boolean res;
        if(isDirectory(filename)){
            res = deleteDirectory(new File(filename));
        } else {
            res = deleteFile(filename);
        }
        return res;
    }

    private Boolean isDirectory(String filename) {
        Boolean isDir = null;
        Path path = Paths.get(filename);
        BasicFileAttributeView basicView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        try {
            BasicFileAttributes basicAttribs = basicView.readAttributes();
            isDir = basicAttribs.isDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(isDir == null) {logger.info("сant check if file is dir, maybe basicAttribs did not found or incorrect filename ");}
        return isDir;
    }

    private boolean deleteFile(String path) {
        //logger.info("deleteFile(String path)");
        File file = new File(path);
        if (file.exists()) {
            //logger.info("file exists, deleting files");
            file.delete();
            return true;
        } else {
            logger.error("I cannot find '" + file + "' ('" + file.getAbsolutePath() + "')");
            return false;
        }
    }

    private boolean deleteDirectory(File path) {
        //logger.info("deleteDirectory(File path)");
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    //logger.info("delete subdirectory");
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }


    private void setPermissions(Path path, byte access){//TODO ONLY FOR WINDOWS!

        boolean exall = (((access >> 0) & 1)==1) ? true : false;
        boolean exown = (((access >> 1) & 1)==1) ? true : false;
        boolean wrall = (((access >> 2) & 1)==1) ? true : false;
        boolean wrown = (((access >> 3) & 1)==1) ? true : false;
        boolean reall = (((access >> 4) & 1)==1) ? true : false;
        boolean reown = (((access >> 5) & 1)==1) ? true : false;

        //"110101"
        //logger.info(Boolean.toString(exall));//true
        //logger.info(Boolean.toString(exown));//false
        //logger.info(Boolean.toString(wrall));//true
        //logger.info(Boolean.toString(wrown));//false
        //logger.info(Boolean.toString(reall));//true
        //logger.info(Boolean.toString(reown));//true

        try {
            Files.setAttribute(path, "dos:readonly", false);

            File file = new File(String.valueOf(path));
            if(file.exists()){
                file.setReadable(reall, reown);
                file.setWritable(wrall, wrown);
                file.setExecutable(exall, exown);
            }
            Boolean isReadable = Files.isReadable(path);
            logger.info("Is file readable: " + isReadable);
            Boolean isWritable = Files.isWritable(path);
            logger.info("Is file writable: " + isWritable);
            Boolean isExecutable = Files.isExecutable(path);
            logger.info("Is file executable: " + isExecutable);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setPermissionsACL(Path path, byte access) {
        //TODO via ACL
        //https://stackoverflow.com/questions/58096912/how-to-set-file-permissions-using-java-nio2-on-windows/58102635#58102635
        //https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/nio/file/attribute/AclFileAttributeView.html
    }

    private void setPermissionsPosix(Path path, byte access) {

        char exall = (((access >> 0) & 1)==1) ? 'e' : '-';
        char exown = (((access >> 1) & 1)==1) ? 'e' : '-';
        char wrall = (((access >> 2) & 1)==1) ? 'w' : '-';
        char wrown = (((access >> 3) & 1)==1) ? 'w' : '-';
        char reall = (((access >> 4) & 1)==1) ? 'r' : '-';
        char reown = (((access >> 5) & 1)==1) ? 'r' : '-';

        char[] perm = new char[9];
        perm[0] = reall;
        perm[0] = wrall;
        perm[0] = exall;
        perm[0] = reown;
        perm[0] = wrown;
        perm[0] = exown;
        perm[0] = '-';
        perm[0] = '-';
        perm[0] = '-';

        String permissions = perm.toString();
        //logger.info(permissions);
        try {
            Files.setPosixFilePermissions(path, PosixFilePermissions.fromString(permissions));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TextEdition createTempFile(TextEdition textEdition) {
        String fileName = textEdition.getFileName();
        logger.debug("fileName="+fileName);
        String edittmp = "";
        Path path = Paths.get(edittmp);
        logger.debug("path="+path);
        Path fileToCreatePath = path.resolve(fileName);
        logger.debug("fileToCreatePath="+fileToCreatePath);
        Path originalPath = path.resolve(getRootDirectory() + File.separator + fileName);
        logger.debug("originalPath="+originalPath);
        File originalFile = null;
        File temporalFile = null;
        try {
            originalFile = FileUtils.getFile(originalPath.toString());
            temporalFile = File.createTempFile("fdeditor-", ".tmp");//C:\Users\Ostap\AppData\Local\Temp\
            logger.debug(temporalFile.getAbsolutePath() + "");
            FileUtils.copyFile(originalFile, temporalFile);
            logger.debug(temporalFile.getAbsolutePath() + "" + temporalFile.getTotalSpace());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(originalFile == null || temporalFile == null) { }

        Path tmpPath =path.resolve(temporalFile.getAbsolutePath());
        logger.debug("tmpPath="+tmpPath);
        textEdition.setTmpFileName(tmpPath);
        String text = readFile(originalFile);
        logger.debug("text="+text);
        textEdition.setText(text);

        return textEdition;
    }

    @Override
    public TextEdition replaceFileByTempFile(TextEdition textEdition) {
//        logger.debug("replaceFileByTempFile="+textEdition.getFileName() + textEdition.getTmpFileName());
        File originalFile = FileUtils.getFile(getRootDirectory() + File.separator + textEdition.getFileName().toString());
        File temporalFile = FileUtils.getFile(textEdition.getTmpFileName().toString());
//        logger.debug("1 readFile(originalFile)"+readFile(originalFile));
//        logger.debug("1 readFile(temporalFile)"+readFile(temporalFile));
//        logger.debug("originalFile.getAbsolutePath()="+originalFile.getAbsolutePath());
//        logger.debug("temporalFile.getAbsolutePath()="+temporalFile.getAbsolutePath());
        try {
            FileUtils.copyFile(temporalFile, originalFile);
        } catch (IOException e) {
            logger.error("can not save original file from temporal");
            e.printStackTrace();
        }

        logger.debug("2 readFile(originalFile)"+readFile(originalFile));
        logger.debug("2 readFile(temporalFile)"+readFile(temporalFile));
        deleteFileOrDir(textEdition.getTmpFileName().toString());
        return textEdition;
    }

    public TextEdition updateTempFile(TextEdition textEdition, TextEdition curTextEdition) {
        File temporalFile = FileUtils.getFile(textEdition.getTmpFileName().toString());
        logger.debug("temporalFile.getAbsolutePath()="+temporalFile.getAbsolutePath());
        return writeSmthToFile(temporalFile, textEdition, curTextEdition);
    }

    private TextEdition writeSmthToFile (File file, TextEdition textEdition, TextEdition curTextEdition){
        String text = curTextEdition.getText();
        StringBuilder sb = new StringBuilder(text);

        for (EditWorkUnit e : textEdition.getEditWorkUnits()) {
            logger.debug("" + e.getAction());
            logger.debug("" + e.getPosition());
            logger.debug("" + e.getKey());
            switch (e.getAction()){
                case "key":
                    String beg = sb.substring(0, e.getPosition());
                    String end = sb.substring(e.getPosition(), sb.length());
                    String insert = e.getKey().toString();
                    logger.debug("beg" + beg);
                    logger.debug("end" + end);
                    logger.debug("insert" + insert);
                    sb = new StringBuilder(beg+insert+end);
                    break;
                case "backSpace":
                    if(e.getPosition()>0) {
                        sb.delete(e.getPosition() - 1, e.getPosition());
                    }
                    break;
                default: break;
            }
        }
        writeFile(file, sb.toString());
        textEdition.setText(sb.toString());
        logger.debug("setText sb.toString()="+sb.toString());
        return textEdition;
    }

    private void writeFile (File file, String text){
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file.getAbsolutePath());
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.print(text);
            printWriter.close();
        } catch (IOException e) {
            logger.error("Can not write to file");
            e.printStackTrace();
        }
    }

    private String readFile (File file){
        StringBuilder sb = new StringBuilder();
        try {
            Files.lines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8).forEach((String s) -> {
                logger.debug(s);
                sb.append(s);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }

    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    public Environment getEnvironment() {
        return environment;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public CurrentDirectory getCurrentDirectory() {
        return currentDirectory;
    }
}