package ru.javabit.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;

import javax.annotation.Resource;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation provides data access to database and filesystem
 * provides info about files by DFile using <tt>DFile</tt> entities, and filesystem info
 *
 * @author  Ostap Strashevskii
 * @see     ru.javabit.controller.DFileController
 * @see     DFile
 * @see     DFileDao
 * @see     ru.javabit.service.DFileService
 * @see     ru.javabit.service.impl.DFileServiceImpl
 *
 *
 * TODO - use File.pathSeparator
 * TODO - use constants
 */

public class DFileDaoImpl extends BasicDaoImpl<DFile> implements DFileDao {

    Logger logger = LoggerFactory.getLogger(DFileDaoImpl.class);

    public DFileDaoImpl(Class<DFile> entityClass) {
        super(entityClass);
    }

    /**
     * Get DFile entities from database
     * @param name is the root directory(path) of files
     */
    @Override
    public List<DFile> getAllByName(String name) {
        List<DFile> dFileList = getSession().createQuery("from DFile where name LIKE ?1")
                .setParameter(1, name).list();
        return dFileList;
    }

    /**
     * Search for DFile entities in database which are the first childs of path directory
     * @param   path is the root directory of files
     */
    public List<Object[]> findChildDFiles(String path) {
        path = path.replace("\\", "\\\\");
        return getSession().createQuery("select d.name, d.comment, d.directory, c.commentText from DFile d left join d.commentList c where d.name like ?1 and d.name not like ?2 ")
                .setParameter(1, path + "%")
                .setParameter(2, path + "%\\\\%")
                .list();
        //SELECT a, b FROM Author a JOIN a.books b

    }

    /**
     * Fill <tt>Map</tt> fileMap with the info DFiles which are absent in dataBase, but exist in filesystem
     * @param  fileMap is the <tt>Map</tt>, where first <tt>String</tt> is filepath, descond- <tt>DFile</tt> entity
     * @return <tt>Map</tt> with files which are absent in dataBase
     */
    public void insertDFiles(Map<String, DFile> fileMap) {
        if (fileMap.size() > 0){
            List<DFile> toInsertlist = new ArrayList<DFile>(fileMap.values());
            for(DFile dFile : toInsertlist) {
                //logger.info("dFile.getName()="+dFile.getName());
                getSession().save(dFile);
            }
        }
    }

    /**
     * Deletes unexisted in filesystem files from dataBase
     * @param  dFileListToDel is the <tt>List</tt>, of Strings filenames which we need to delete
     */
    public void deleteDFiles(List<String> dFileListToDel) {
        if (dFileListToDel.size() > 0) {
            int i = getSession().createQuery("delete from DFile where name in (?1)")
                    .setParameterList(1, dFileListToDel).executeUpdate();
            logger.info("deleted=" + i);
        }
    }

    @Override
    public List<Comment> addCommentAndGetAll(String name, String commentText) {
        Comment resComment = new Comment();
        resComment.setName(name);
        resComment.setCommentText(commentText);
        resComment.setLastEdited(LocalDate.now());
        DFile resDFile = this.getByName(name);
        resDFile.getCommentList().add(resComment);
        getSession().save(resDFile);
        return resDFile.getCommentList();
    }
}