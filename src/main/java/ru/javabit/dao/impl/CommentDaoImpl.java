package ru.javabit.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javabit.dao.CommentDao;
import ru.javabit.entity.Comment;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CommentDaoImpl extends BasicDaoImpl<Comment> implements CommentDao {

    Logger logger = LoggerFactory.getLogger(DFileDaoImpl.class);

    public CommentDaoImpl(Class<Comment> entityClass) {
        super(entityClass);
    }

    @Override
    public Comment addCommentToDFile(String name, String commentText) {
        Comment resComment = new Comment();
        resComment.setName(name);
        resComment.setCommentText(commentText);
        resComment.setLastEdited(LocalDate.now());
        getSession().save(resComment);
        return resComment;
    }

    @Override
    public List<Comment> getDFileComments(String name) {
            List<Comment> comments = new ArrayList<>(getSession().createQuery("FROM Comment WHERE name = ?1")
                    .setParameter(1, name).list());
            //logger.error("CommentDaoImpl.getDFileComments() name=" + name + " comments.size()="+comments.size());
            return comments;
    }

    @Override
    public List<Comment> addCommentAndGetAll(String name, String commentText) {
        Comment resComment = new Comment();
        resComment.setName(name);
        resComment.setCommentText(commentText);
        resComment.setLastEdited(LocalDate.now());
        getSession().save(resComment);
        List<Comment> comments = new ArrayList<>(getSession().createQuery("FROM Comment WHERE name = ?1")
                .setParameter(1, name).list());
        return comments;
    }
//    @Override
//    public void add(Comment comment) {
//        Session session = getSession();
////        dFile.getCommentList().add(comment);
//        Transaction tx = session.beginTransaction();
//        session.save(comment);
//        tx.commit();
//    }
}