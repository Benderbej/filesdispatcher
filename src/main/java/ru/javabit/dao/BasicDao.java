package ru.javabit.dao;

import java.util.List;
import java.util.Set;

public interface BasicDao<T> {

    T add(T entity);

    T update(T entity);

    T delete(T entity);

    T deleteByName(String name);

    T getById(long id);

    T getByName(String name);

    List<T> getAllByName(String name);

    List<T> getAll();

    Set<T> getAllSet();
}