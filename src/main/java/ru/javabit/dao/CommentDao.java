package ru.javabit.dao;

import ru.javabit.dao.impl.BasicDaoImpl;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.util.List;
import java.util.Map;

public interface CommentDao extends BasicDao<Comment> {

    Comment addCommentToDFile(String name, String commentText);

    List<Comment> getDFileComments(String name);

    List<Comment> addCommentAndGetAll(String name, String commentText);
}
