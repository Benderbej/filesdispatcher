package ru.javabit.entity;

/**
 * common Entity for both files & directories
 *
 * file description in DB
 *
 * @property name - full file name with the path
 */

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "files")
public class DFile implements HaveName {
    @Id
    private String name;
    @Column(name = "isdirectory")
    private Boolean directory;
    private String comment;
    @Column(columnDefinition = "DATE")
    private LocalDate lastUpdated;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "name")
    private List<Comment> commentList = new ArrayList<>();

    private String extensionClassName;//need for frontend TODO by DTO

    private String editableClass;//need for frontend TODO by DTO

    public DFile() {}

    public DFile(String name, String comment, LocalDate lastUpdated, boolean isDirectory) {
        this.name = name;
        this.comment = comment;
        this.lastUpdated = lastUpdated;
        this.directory = isDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Boolean getDirectory() {
        return directory;
    }

    public void setDirectory(Boolean directory) {
        directory = directory;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getExtensionClassName() {
        return extensionClassName;
    }

    public void setExtensionClassName(String extensionClassName) {
        this.extensionClassName = extensionClassName;
    }

    public String getEditableClass() {
        return editableClass;
    }

    public void setEditableClass(String editableClass) {
        this.editableClass = editableClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DFile)) return false;
        DFile dFile = (DFile) o;
        if (!getName().equals(dFile.getName())) return false;
        return directory.equals(dFile.directory);
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + directory.hashCode();
        return result;
    }
}