package ru.javabit.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "comments")
public class Comment implements HaveName {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String name;
    @Column(name = "text")
    private String commentText;
    @Column(columnDefinition = "DATE")
    private LocalDate lastEdited;
    @ManyToOne
    private DFile dFile;

    public Comment() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public LocalDate getLastEdited() {
        return lastEdited;
    }

    public void setLastEdited(LocalDate lastEdited) {
        this.lastEdited = lastEdited;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DFile getdFile() {
        return dFile;
    }

    public void setdFile(DFile dFile) {
        this.dFile = dFile;
    }
}