package ru.javabit.entity;

import javax.persistence.Entity;

/**
 * to create file in file system
 *
 * file description in file system
 */

public class FilePattern {
    private String fileName;
    private boolean isFile;
    private Byte access;

    public FilePattern () {}

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public Byte getAccess() {
        return access;
    }

    public void setAccess(Byte access) {
        this.access = access;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FilePattern)) return false;

        FilePattern that = (FilePattern) o;

        if (isFile() != that.isFile()) return false;
        if (!getFileName().equals(that.getFileName())) return false;
        return getAccess().equals(that.getAccess());
    }

    @Override
    public int hashCode() {
        int result = getFileName().hashCode();
        result = 31 * result + (isFile() ? 1 : 0);
        result = 31 * result + getAccess().hashCode();
        return result;
    }
}