package ru.javabit.entity;

public interface HaveName<T> {

    String getName();

    void setName(String s);
}
