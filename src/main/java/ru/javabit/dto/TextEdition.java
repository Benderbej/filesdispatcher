package ru.javabit.dto;

import org.springframework.stereotype.Component;

import java.nio.file.Path;

/**
 *
 * DTO object, describing Json-file with information about current events
 * @property text
 * @property editorsNum - num of the editors editing this file
 * @property status -
 * 0 initialisation of editing file
 * 1 editing in progress
 * 2 the end of editing is triggering
 */

//@Component
public class TextEdition {

    private String fileName;
    private Path tmpFileName;
    private String text;
    private int cursorPos;
    private int editorsNum;
    private int status;
    private EditWorkUnit[] editWorkUnits;

    public TextEdition(String fileName, Path tmpFileName, String text, int cursorPos, int editorsNum, int status) {
        this.fileName = fileName;
        this.tmpFileName =  tmpFileName;
        this.text = text;
        this.editorsNum = editorsNum;
        this.status = status;
        this.cursorPos = cursorPos;
    }

    public TextEdition(){}

    public int getEditorsNum() {
        return editorsNum;
    }

    public void setEditorsNum(int editorsNum) {
        this.editorsNum = editorsNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getCursorPos() {
        return cursorPos;
    }

    public void setCursorPos(int cursorPos) {
        this.cursorPos = cursorPos;
    }

    public Path getTmpFileName() {
        return tmpFileName;
    }

    public void setTmpFileName(Path tmpFileName) {
        this.tmpFileName = tmpFileName;
    }

    public EditWorkUnit[] getEditWorkUnits() {
        return editWorkUnits;
    }

    public void setEditWorkUnits(EditWorkUnit[] editWorkUnits) {
        this.editWorkUnits = editWorkUnits;
    }
}

