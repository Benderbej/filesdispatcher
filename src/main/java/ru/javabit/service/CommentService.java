package ru.javabit.service;

import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.util.List;

public interface CommentService {

    Comment add(Comment comment);

    void addCommentToDFile(String name, String commentText);

    List<Comment> getAllCommentsByName(String name);

    List<Comment> addCommentAndGetAll(Comment comment);
}