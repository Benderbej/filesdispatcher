package ru.javabit.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.javabit.entity.DFile;

@Service
public interface FileService {

    DFile uploadFile(MultipartFile file);

    DFile createFile(String filename, boolean isFile, byte access);

    boolean deleteFileOrDir(String filename);
}