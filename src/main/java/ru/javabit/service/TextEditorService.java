package ru.javabit.service;

import ru.javabit.dto.TextEdition;

public interface TextEditorService {

    TextEdition startNewFileEditing(TextEdition textEdition);

    TextEdition saveFileAndOut(TextEdition textEdition);

    TextEdition updateFile(TextEdition textEdition);

    TextEdition processTextEdition(TextEdition textEdition);
}
