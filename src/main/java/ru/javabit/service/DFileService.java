package ru.javabit.service;

import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;

import java.util.List;

public interface DFileService {
    DFile add(DFile dFile);

    DFile update(DFile dFile);

    DFile delete(String name);

    List<DFile> getAllDFiles();

    List<DFile> getAllChildByName(String name);

    DFile getByName(String name);

    DFile getParentName(String name);

    List<Comment> addCommentAndGetAll(Comment comment);
}