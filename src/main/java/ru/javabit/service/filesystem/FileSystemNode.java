package ru.javabit.service.filesystem;

import com.sun.jmx.mbeanserver.DefaultMXBeanMappingFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.javabit.controller.AppController;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;

import javax.annotation.Resource;
import java.io.File;
import java.time.LocalDate;
import java.util.*;

/**
 * this is utility class-helper to DFileServiceImpl, provides compatibility database-filesystem
 * @property path - String, current node filepath
 * @property dFileActualChildList - DFiles from database
 * @property dFileListToDel - collection to delete files from database (have not allready existed untill the moment of scan)
 * @property fileMap - collection of DFile entities to persist
 */

public class FileSystemNode {

    Logger logger = LoggerFactory.getLogger(FileSystemNode.class);

    private Environment environment;

    private String rootDirectory;

    private List<Object[]> dFileChildDataBaseList;
    Set<String> DBNamesSet;
    Map<String, DFile> dBMap;

    Map<String, DFile> addIntoDBMap;
    Map<String, DFile> removeFromDBMap;
    Map<String, DFile> updateInDBMap;
    Map<String, DFile> actualMap;

    List<DFile> dFileActualChildList = new ArrayList<>();
    private List<String> dFileListToDel = new ArrayList<>();
    private Map<String, DFile> fileMap;
    String path;

    public FileSystemNode(List<Object[]> dFileChildDataBaseList, String path, String rootDirectory){
        this.rootDirectory = rootDirectory;
        this.path = path;
        this.dFileChildDataBaseList = dFileChildDataBaseList;
        getDFilesNamesFromDBResList();
        getDBMapFromDBResList();
    }

    /**
     * Scan filesystem and compare it with database info, prepare collections for update and delete queries from database, provide compatibility between filesystem and database,
     * fill actual list of node's childs
     *
     * for each file in DB check if it exist in filesystem
     * if exist and equal - skip it
     * if does not - add it to insertion list
     */
    public void scanFileSystemNode() {
        fileMap = fillFileMap(path);
        prepareMapsToRemoveAndAdd();
        prepareActualMap();
        prepareUpdateMap();
        logger.debug("addIntoDBMap.size()="+addIntoDBMap.size());
        logger.debug("removeFromDBMap.size()="+removeFromDBMap.size());
        bridge();//BRIDGE METHOD
    }

    private void prepareUpdateMap() {
        for (String name : fileMap.keySet()) {//for each file in filesystem (node)
            if((dBMap.keySet().contains(name))) {//if there is an entry for both file and db
                if (dBMap.get(name).getDirectory() != fileMap.get(name).getDirectory()) {//if it change file/dir prop
                    DFile dFile = dBMap.get(name);
                    dFile.setDirectory(fileMap.get(name).getDirectory());
                    updateInDBMap.put(name, dFile);
                }
            }
        }
    }

    private void prepareActualMap() {
        actualMap = new HashMap<>(dBMap);
        actualMap.putAll(addIntoDBMap);
        actualMap.keySet().removeAll(removeFromDBMap.keySet());
    }

    private void bridge() {
        fileMap = addIntoDBMap;
        dFileActualChildList.addAll(actualMap.values());
        dFileListToDel.addAll(removeFromDBMap.keySet());
        logger.debug("dFileActualChildList.size()="+dFileActualChildList.size());
    }

    private void prepareMapsToRemoveAndAdd() {
        logger.debug("fileMap.size()="+fileMap.size());
        logger.debug("dBMap.size()="+dBMap.size());
        addIntoDBMap = new HashMap<>(fileMap);
        addIntoDBMap.keySet().removeAll(dBMap.keySet());
        removeFromDBMap = new HashMap<>(dBMap);
        removeFromDBMap.keySet().removeAll(fileMap.keySet());
    }

    private void getDFilesNamesFromDBResList (){
        DBNamesSet = new HashSet<>();
        for (Object[] obj : dFileChildDataBaseList) {
            String name = (String) obj[0];
            DBNamesSet.add(name);
        }
    }

    private void getDBMapFromDBResList (){
        dBMap = new HashMap<>();
        for (Object[] obj : dFileChildDataBaseList) {
            String name = (String) obj[0];
            String comment = (String) obj[1];
            Boolean isDirectory = (Boolean) obj[2];
            dBMap.put(name, new DFile(name, comment, LocalDate.now(), isDirectory));
        }
    }

    /**
     * Fill <tt>HashMap</tt> with the info about files from filesystem
     * scans filesystem and generate map with current info about every file
     * @param   path is the root directory of files
     */
    public Map<String, DFile> fillFileMap(String path) {

        int rootPathLength = rootDirectory.length();
        Map<String, DFile> fileMap = new HashMap<>();
        File dir = new File(path);
        if (dir.isDirectory()) {
            File[] fileArr = dir.listFiles();
            for (File f : fileArr) {
                DFile dFile;

                int l = f.getPath().length();
                String p = f.getPath().substring(rootPathLength, l);
                logger.debug("p="+p);

                if(f.isDirectory()){

                    dFile = new DFile(f.getPath(), "" , null, true);
                    //dFile = new DFile(p, "" , null, true);
                } else {


                    dFile = new DFile(f.getPath(), "" , null, false);
                    //dFile = new DFile(p, "" , null, false);
                }
                fileMap.put(f.getPath(), dFile);
            }
        }
        return fileMap;
    }

    public List<DFile> getdFileActualChildList() {
        return dFileActualChildList;
    }

    public List<String> getdFileListToDel() {
        return dFileListToDel;
    }

    public Map<String, DFile> getFileMap() {
        return fileMap;
    }










//
//    public String getRootDirectory() {
//        if(environment == null){logger.error("environment");}
//        environment = getEnvironment();
//        if(environment == null){logger.error("environment");}
//        logger.error("environment.getRequiredProperty(filedispatcher.root)" + environment.getRequiredProperty("filedispatcher.root"));
//
//        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
//    }
//
//    public void setRootDirectory(String rootDirectory) {
//        this.rootDirectory = rootDirectory;
//    }
//
//    public Environment getEnvironment() {
//        return environment;
//    }
//
//    @Autowired
//    public void setEnvironment(Environment environment) {
//        this.environment = environment;
//    }
}