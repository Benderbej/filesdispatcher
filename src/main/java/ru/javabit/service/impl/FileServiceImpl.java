package ru.javabit.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.javabit.dao.DFileDao;
import ru.javabit.dao.FilePatternDao;
import ru.javabit.entity.DFile;
import ru.javabit.service.FileService;

import java.io.File;

@Service("fileService")
public class FileServiceImpl implements FileService {

    Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    private DFileDao dFileDao;
    private FilePatternDao filePatternDao;

    public DFile uploadFile(MultipartFile file) {
        logger.error("uploadFile");
        DFile dFile = filePatternDao.uploadFile(file);
        if(dFile != null){
            dFileDao.add(dFile);
        } else {
            logger.error("can't createFile()");
        }
        return dFile;
    }

    public DFile createFile( String filename, boolean isDir, byte access) {
        logger.error("filename"+filename);
        DFile dFile = filePatternDao.createFile(filename, isDir, access);
        if(dFile != null){
            dFileDao.add(dFile);
        } else {
            logger.error("can't createFile()");
        }
        return dFile;
    }

    @Override
    public boolean deleteFileOrDir(String filename) {
        if(filePatternDao.deleteFileOrDir(filename)){
            dFileDao.deleteByName(filename);
            return true;
        } else {
            logger.error("can't deleteFileOrDir()");
            return false;
        }
    }

    @Autowired
    public void setDFileDao(DFileDao dFileDao) {
        this.dFileDao = dFileDao;
    }

    @Autowired
    public void setFileDao(FilePatternDao filePatternDao) {
        this.filePatternDao = filePatternDao;
    }
}