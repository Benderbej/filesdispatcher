package ru.javabit.service.impl;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.javabit.dao.CommentDao;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import ru.javabit.filesdispatch.CurrentDirectory;
import ru.javabit.service.DFileService;
import ru.javabit.service.filesystem.FileSystemNode;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service("dfileService")
public class DFileServiceImpl implements DFileService {

    Logger logger = LoggerFactory.getLogger(DFileServiceImpl.class);

    private DFileDao dFileDao;

    private CommentDao commentDao;

    private Environment environment;
    private String rootDirectory;

    @Resource(name = "initCurrentDirectory")
    CurrentDirectory currentDirectory;

    @Override
    public DFile add(DFile dFile) {
        return dFileDao.add(dFile);
    }

    @Override
    public DFile update(DFile dFile) {
        return dFileDao.update(dFile);
    }

    @Override
    public DFile delete(String name) {
        return dFileDao.delete(getByName(name));
    }

    @Override
    public DFile getByName(String name) {
        return dFileDao.getByName(name);
    }

    @Override
    public List<DFile> getAllDFiles() {
        return dFileDao.getAll();
    }

    @Override
    public List<DFile> getAllChildByName(String name) {
        FileSystemNode fileSystemNode = new FileSystemNode(dFileDao.findChildDFiles(name), name, getRootDirectory());
        fileSystemNode.scanFileSystemNode();
        List<DFile> dFileList = new ArrayList<>();
        for (DFile dFile : fileSystemNode.getdFileActualChildList()) {
            DFile dbDFile = dFileDao.getByName(dFile.getName());
            dFileList.add(dbDFile);
        }
        dFileDao.deleteDFiles(fileSystemNode.getdFileListToDel());
        dFileDao.insertDFiles(fileSystemNode.getFileMap());
        return getActualInfoFromDBaseToDFileList(fileSystemNode.getdFileActualChildList());
    }

    @Override
    public DFile getParentName(String name) {
        name = name.substring(0, name.length()-1);
        if (name.equals(getRootDirectory()) || (name == null) || name.equals("") || name.equals("/")) {
            logger.debug("getParentName="+name);
            return new DFile();
        }
        int lastSlashPos = name.lastIndexOf('\\');
        if (lastSlashPos >= 0) {
            name=name.substring(0, lastSlashPos);
            if(dFileDao.getByName(name) != null){
                if (dFileDao.getByName(name).getName().equals(currentDirectory.getCurrentDirUrl())) {
                    return new DFile();
                }
                return dFileDao.getByName(name);
            }

            return new DFile(name, "" , null, true);
        } else {
            logger.debug("getParentName="+name);
            return new DFile();
        }
    }

    @Override
    public List<Comment> addCommentAndGetAll(Comment comment) {
        return dFileDao.addCommentAndGetAll(comment.getName(), comment.getCommentText());
    }

    @Autowired
    public void setDFileDao(DFileDao dFileDao) {
        this.dFileDao = dFileDao;
    }

    @Autowired
    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public List<DFile> getActualInfoFromDBaseToDFileList (List<DFile> dFileList) {
        List<DFile> resDFileList = new ArrayList<>();
        for (DFile dFile : dFileList) {
            DFile d = dFileDao.getByName(dFile.getName());
            List<Comment> lc = commentDao.getDFileComments(dFile.getName());
            List<Comment> lc2 = d.getCommentList();
            d.setCommentList(commentDao.getDFileComments(dFile.getName()));
            resDFileList.add(d);
        }
        return resDFileList;
    }
}