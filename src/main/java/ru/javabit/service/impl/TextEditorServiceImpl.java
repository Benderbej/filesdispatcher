package ru.javabit.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.javabit.dao.DFileDao;
import ru.javabit.dao.FilePatternDao;
import ru.javabit.dto.TextEdition;
import ru.javabit.filesdispatch.CurrentFilesOnEdit;
import ru.javabit.service.TextEditorService;

import javax.annotation.Resource;
import java.io.File;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Service("textEditorService")
public class TextEditorServiceImpl implements TextEditorService {

    Logger logger = LoggerFactory.getLogger(TextEditorServiceImpl.class);

    @Resource(name = "initCurrentFilesOnEdit")
    CurrentFilesOnEdit currentFilesOnEdit;

    private DFileDao dFileDao;
    private FilePatternDao filePatternDao;
    private Environment environment;
    private String rootDirectory;

    @Override
    public TextEdition startNewFileEditing(TextEdition textEdition) {
        logger.error("startNewFileEditing textEdition.getFileName()="+textEdition.getFileName());

        String originalName = getRootDirectory() + File.separator + textEdition.getFileName();
        if(getCurrentFilesOnEdit().getTextEditionMap().containsKey(originalName)){
            logger.error("one more user editing="+textEdition.getFileName());
            textEdition = getCurrentFilesOnEdit().getTextEditionMap().get(originalName).get();
            textEdition.setEditorsNum(textEdition.getEditorsNum()+1);
            setTextEdition(textEdition);

        } else {
            logger.error("new user editing="+textEdition.getFileName());
            textEdition = newFileEditingInit(textEdition);
        }
        return textEdition;
    }

    @Override
    public TextEdition updateFile(TextEdition textEdition) {
        String originalName = getRootDirectory() + File.separator + textEdition.getFileName();
        logger.debug("TEXT SENDING="+textEdition.getText());
        logger.debug("TEXT EXISTING="+getCurrentFilesOnEdit().getTextEditionMap().get(originalName).get().getText());
        textEdition = filePatternDao.updateTempFile(textEdition, getCurrentFilesOnEdit().getTextEditionMap().get(originalName).get());
        filePatternDao.replaceFileByTempFile(textEdition);//вот и все автосохранение =( одну строку просто добавить нужно было, все методы есть для этого
        setTextEdition(textEdition);
        TextEdition te = getCurrentFilesOnEdit().getTextEditionMap().get(originalName).get();
        logger.debug("RES="+te.getText());
        return textEdition;
    }

    @Override
    public TextEdition saveFileAndOut(TextEdition textEdition) {
        String originalName = getRootDirectory() + File.separator + textEdition.getFileName();
        textEdition = filePatternDao.updateTempFile(textEdition, getCurrentFilesOnEdit().getTextEditionMap().get(originalName).get());
        textEdition.setEditorsNum(textEdition.getEditorsNum() - 1);
        textEdition = removeTextEdition(textEdition);
        return textEdition;
    }

    @Override
    public TextEdition processTextEdition(TextEdition textEdition) {
        int status = textEdition.getStatus();
        TextEdition te = textEdition;
        switch (status){
            case 0:
                te = startNewFileEditing(textEdition);
                break;
            case 1:
                te = updateFile(textEdition);
                break;
            case 2:
                te = saveFileAndOut(textEdition);
                break;
            default:
                Exception ex = new Exception("illegal textEdition status! ");
                try {
                    throw ex;
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return te;
    }

    private TextEdition newFileEditingInit(TextEdition textEdition) {
        textEdition.setEditorsNum(1);
        textEdition.setStatus(1);
        textEdition = filePatternDao.createTempFile(textEdition);
        setTextEdition(textEdition);
        return textEdition;
    }

    private void setTextEdition(TextEdition textEdition){
        Map textEditionMap = getCurrentFilesOnEdit().getTextEditionMap();
        AtomicReference<TextEdition> aTextEdition = new AtomicReference<>();//TODO refactor - remove init new obj
        aTextEdition.set(textEdition);
        textEditionMap.put(getRootDirectory() + File.separator + textEdition.getFileName().toString(), aTextEdition);
    }

    private TextEdition removeTextEdition(TextEdition textEdition){
        TextEdition res = textEdition;
        if(textEdition.getEditorsNum() <= 0){
            logger.error("one less user editing="+textEdition.getFileName());
            res = filePatternDao.replaceFileByTempFile(textEdition);
            Map textEditionMap = getCurrentFilesOnEdit().getTextEditionMap();
            textEditionMap.remove(getRootDirectory() + File.separator + textEdition.getFileName().toString());
        }
        return res;
    }

    @Autowired
    public void setDFileDao(DFileDao dFileDao) {
        this.dFileDao = dFileDao;
    }

    @Autowired
    public void setFileDao(FilePatternDao filePatternDao) {
        this.filePatternDao = filePatternDao;
    }

    public CurrentFilesOnEdit getCurrentFilesOnEdit() {
        return currentFilesOnEdit;
    }

    public String getRootDirectory() {
        return rootDirectory = environment.getRequiredProperty("filedispatcher.root").replaceAll("\"|\'|;", "");
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}