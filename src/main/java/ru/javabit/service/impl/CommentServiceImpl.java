package ru.javabit.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.javabit.controller.CommentController;
import ru.javabit.dao.CommentDao;
import ru.javabit.dao.DFileDao;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import ru.javabit.service.CommentService;

import java.time.LocalDate;
import java.util.List;

@Service("commentService")
public class CommentServiceImpl implements CommentService {

    Logger logger = LoggerFactory.getLogger(CommentServiceImpl.class);

    private CommentDao commentDao;
    private DFileDao dFileDao;

    @Override
    public Comment add(Comment comment) {
        String name = comment.getName();
        DFile dFile = dFileDao.getByName(name);
        comment.setLastEdited(LocalDate.now());
        dFile.getCommentList().add(comment);
        dFileDao.update(dFile);
        return comment;
    }

    @Override
    public List<Comment> addCommentAndGetAll(Comment comment) {
        String name = comment.getName();
        DFile dFile = dFileDao.getByName(name);
        comment.setLastEdited(LocalDate.now());
        List<Comment> commentList = dFile.getCommentList();
        commentList.add(comment);
        dFile.setCommentList(commentList);
        dFileDao.update(dFile);
        String namee = dFile.getName();
        return commentDao.getDFileComments(namee);
    }

    @Override
    public void addCommentToDFile(String name, String commentText) {
        commentDao.addCommentToDFile(name, commentText);
    }

    @Override
    public List<Comment> getAllCommentsByName(String name) {
        return commentDao.getDFileComments(name);
    }

    @Autowired
    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Autowired
    public void setDFileDao(DFileDao dFileDao) {
        this.dFileDao = dFileDao;
    }
}