package ru.javabit.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import ru.javabit.dao.CommentDao;
import ru.javabit.dao.DFileDao;
import ru.javabit.dao.FilePatternDao;
import ru.javabit.dao.impl.CommentDaoImpl;
import ru.javabit.dao.impl.DFileDaoImpl;
import ru.javabit.dao.impl.FilePatternDaoImpl;
import ru.javabit.entity.Comment;
import ru.javabit.entity.DFile;
import ru.javabit.entity.FilePattern;
import ru.javabit.filesdispatch.CurrentDirectory;
import ru.javabit.filesdispatch.CurrentFilesOnEdit;

import java.util.Locale;

@Configuration
@EnableAspectJAutoProxy
//@PropertySource(value = "classpath:message.properties", encoding = "UTF-8")
//@PropertySource(value = "classpath:application-${env}.properties", encoding = "UTF-8")
public class AppConfig {

    @Bean
    public DFileDao dFileDao() {
        return new DFileDaoImpl(DFile.class);
    }

    @Bean
    public CommentDao commentDao() {
        return new CommentDaoImpl(Comment.class);
    }

    @Bean
    public FilePatternDao filePatternDao() {return new FilePatternDaoImpl(FilePattern.class);}

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public CurrentDirectory initCurrentDirectory() {
        return new CurrentDirectory();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public CurrentFilesOnEdit initCurrentFilesOnEdit() {
        return new CurrentFilesOnEdit();
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(300000);
        return multipartResolver;
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.getDefault());
        return slr;
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setCacheSeconds(3600);
        messageSource.setDefaultEncoding("UTF-8"); // Add this
        return messageSource;
    }
}